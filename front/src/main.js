import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';
const pinia = createPinia();

import ModalForm from './components/common/Modal.vue';
import SlidingSidebar from './components/common/SlidingSidebar.vue';
import TreeItemBase from './components/tree-item/TreeItemBase.vue';
import DragItem from "@/components/common/DragItem.vue";
import AppHeader   from "@/components/common/AppHeader.vue";

// import "./assets/css/font-awesome.min.css"
import "./assets/midone/app.css"
import "./assets/css/table-style.css"
import "./assets/css/tree-style.css"
import "./assets/css/file-ext.css"
import "./assets/css/style.css"

const app = createApp(App)
app.use(router)
app.use(pinia);
app.component('modal-form', ModalForm)
app.component('drag-item', DragItem);
app.component('app-header', AppHeader)
app.component('tree-item-base', TreeItemBase)
app.component('sliding-sidebar', SlidingSidebar)
app.mount('#layout-wrapper')
