import {ref} from "vue";
import api from '@/api/routes.js';
const http = api.http;
import { useStore } from "@/store/index.js";
import useEventEmitter from "@/composables/useEmitter.js";

const CURRENT_PATH_NAME = 'CURRENT_PATH';

function useFileManager() {

    const files = ref([]);
    const response = ref([]);
    const store = useStore();
    const { eventEmit, eventOn } = useEventEmitter()

    const localStore = (key, data = null, remove = false) => {
        if(remove) return localStorage.removeItem(key);
        if(data) return localStorage.setItem(key, data);
        return localStorage.getItem(key);
    }

    const currentPath = (path = null) => {
        return path ? localStore(CURRENT_PATH_NAME, path) : localStore(CURRENT_PATH_NAME)
    }

    const copy = (obj) => Object.assign({}, obj)

    const getResult = (resp) => {
        response.value = resp
        const { data, error } = response.value
        return data.result;
    }

    const loadFiles = async (path = '') => {
        const data = await http('getFiles', path);
        const result = getResult(data)
        return result;
    }

    const getFiles = async (path = '') => {
        const data = await http('getFiles', path);
        const result = getResult(data)
        currentPath(path);
        store.files  = copy(result)
        store.currentPath = path;
        files.value  = copy(result)
        return result;
    }

    const getTreeFiles = async (path = '') => {
        const data = await http('getFiles', path);
        const result = getResult(data)
        return result;
    }

    const getRootPaths = async () => {
        const data = await  http('getRootPaths');
        const result = getResult(data)
        store.serverPath = result?.server;
        store.systemPath = result?.system;
        return result;
    }

    const createResource = async (param, callback = null) => {
        param['path'] = store.currentPath;
        const data = await  http('createResource', param);
        const result = getResult(data)
        return result;
    }

    const getSystemFiles = () => {
        return getFiles(store.systemPath);
    }

    const getServerFiles = () => {
        return getFiles(store.serverPath);
    }

    const getFilesToCurrent = () => {
        return getFiles(store.currentPath);
    }

    const getFileContent = async (path) => {
        const data = await http('getFileContent', path);
        const result = getResult(data)
        return result;
    }

    const saveFileContent = async (data) => {
        const response   = await http('saveFileContent', data);
        const result = getResult(response)
        return result;
    }

    const deleteResource = async (path, type) => {
        const data = await http('deleteResource', { path, type });
        const result = getResult(data)
        return result;
    }

    const renameFile = async (data) => {
        const response = await http('renameFile', data);
        const result = getResult(response)
        return result;
    }

    const downloadFile = async (data) => {
        const response = await http('downloadFile', data);
        const result = getResult(response)
        return result;
    }

    const getFtpFiles = async (path) => {
        const response = await http('getFtpFiles', path);
        const result = getResult(response)
        return result;
    }

    const ftpAction = async (action, data) => {
        const response = await http(action, data);
        const result = getResult(response)
        return result;
    }

    const copyFile = async (data) => {
        const response = await http('copyFile', data);
        const result = getResult(response)
        return result;
    }

    const treeRecursiveOpen = (path, files) => {
        const paths = path.split('/');
        for (let i in paths) {
            let pathName = paths[i];
            for(let key in files) {
                let file = files[key];
                if(file.name != pathName) continue;
                files[key]['open'] = path;
                return files;
            }
        }
        return files;
    }

    return {
        store,
        files,
        response,
        getFiles,
        loadFiles,
        localStore,
        getRootPaths,
        getSystemFiles,
        getServerFiles,
        getTreeFiles,
        getFileContent,
        getFilesToCurrent,
        treeRecursiveOpen,
        createResource,
        deleteResource,
        saveFileContent, copyFile,
        renameFile, downloadFile,
        eventEmit, eventOn,
        getFtpFiles, ftpAction,
        apiUrl: api.apiUrl,
    }
}

export default useFileManager;
