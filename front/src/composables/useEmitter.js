import tineEmitter from 'tiny-emitter/instance';

function useEmitter() {

    const sendEvent = (eventName, data = {}) => {
        tineEmitter.emit(eventName, data)
    }

    const listenEvent = (eventName, fn = null) => {
        tineEmitter.on(eventName, fn)
    }

    const eventEmit = (eventName, data) => {
        tineEmitter.emit(eventName, data)
    }

    const eventOn = (eventName, data) => {
        tineEmitter.on(eventName, data)
    }

    return {
        tineEmitter,
        sendEvent,
        listenEvent,
        eventEmit,
        eventOn,
    }
}

export default useEmitter
