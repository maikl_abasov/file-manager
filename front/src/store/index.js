import { defineStore } from 'pinia'

export const useStore = defineStore({

    id: 'store',
    state: () => ({
        serverPath: '',
        systemPath: '',
        currentPath: '',
        copySourcePath: '',
        copyDestPath: '',
        ftpPath: '',
        files: [],

        infoMessage: {
            message: '',
            type: 'success',
            status: false,
        }

        // apiUrl: apiUrl,
        // systemInfo: {},
        // rootPath: '',
        // preloader: false,
        // error: null,
    }),

    getters: {
        getCurrentPathRender() {
            let list = this.currentPath.split('/');
            if(list.length && list[0] == "") list[0] = '/';
            return list;
        },

        getCheckedFiles() {
            return this.files.filter(item => item.checked);
        },
    },

    actions: {},

})
