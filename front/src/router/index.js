import { createRouter, createWebHistory } from 'vue-router'

import Home from '@/pages/Home.vue'
import FileManager from '@/pages/FileManager.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/file-manager',
    name: 'file-manager',
    component: FileManager
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
