import api from "@/api/http.js";

const http = async (name, params = null) => {
    let data = [], error = null;
    try {
        const response = (params) ? await routes[name](params) : await routes[name]();
        data = response.data;
    }
    catch (err) { error = err; }
    return { data , error };
}

const send = api.send;

const routes = {
    getFiles      : (path = '') => send('/scandir?path=' + path, 'get'),
    getRootPaths  :  ()      => send('/get-root-paths', 'get'),
    createResource:  (data)  => send(`/create-resource?path=${data.path}&name=${data.name}&type=${data.type}`, 'get'),
    deleteResource:  (data)  => send(`/delete-resource?path=${data.path}&type=${data.type}`, 'get'),
    getFileContent:  (path)  => send(`/get-file-content?path=${path}`, 'get'),
    saveFileContent: (data)  => send(`/save-file-content?path=${data.path}` , 'post', { content: data.content} ),
    renameFile:      (data)  => send(`/rename-file?path=${data.path}&new_name=${data.newName}&old_name=${data.oldName}`, 'get'),
    downloadFile:    (data)  => send(`/download-file?path=${data.path}&type=${data.type}`, 'get'),
    copyFile: (data)  => send(`/copy-file/${data.action}?source=${data.source}&dest=${data.dest}`, 'get'),

    getFtpFiles     : (path = '') => send(`/ftp/scandir?path=${path}`, 'get'),
    copyToFtp       : (data) => send(`/ftp/copy?ftp_path=${data.ftpPath}&local_path=${data.localPath}`, 'get'),
    downloadFromFtp : (data) => send(`/ftp/download?ftp_path=${data.ftpPath}&local_path=${data.localPath}`, 'get'),
    deleteToFtp     : (path) => send(`/ftp/delete?ftp_path=${path}`, 'get'),

};

export default {
    apiUrl: api.apiUrl,
    api, http, routes
};
