import axios from "axios";

const API_URL = 'http://test.ru/FILE-MANAGER/api';

const HttpClient = axios.create({
    baseURL: API_URL,
    //timeout: 1000,
    //headers: {'X-Custom-Header': 'foobar'}
});

HttpClient.interceptors.request.use(
    config => config,
    error =>  Promise.reject(error)
);

HttpClient.interceptors.response.use(
    response => {
        responseError(response)
        return response
    },
    error => Promise.reject(error)
);

const responseError = (response) => {
    if(response.data?.error) {
        console.log(response);
        lg(response.data);
    }
}

const send = (url, method = 'get', data = null) => {
    return HttpClient[method](url, data);
}

export default  {
    apiUrl: API_URL,
    send,
};



