
// function lg(variable, deep, index) {
//
//     if (variable===null) {var variable = 'null';}
//     if (deep===undefined) {var deep = 0;}
//     if (index===undefined) {var index = '';} else {index+=': ';}
//     var mes = ''; var i = 0; var pre = '\n';
//     while (i<deep) {pre+='\t'; i++;}
//     if (variable && variable.nodeType!=undefined) {
//         mes+=pre+index+'DOM node'+((variable.nodeType==1)? (' <'+variable.tagName+'>'): '');
//     } else if (typeof(variable)=='object') {
//         mes+=pre+index+' {';
//         for (index in variable) {
//             mes+=lg(variable[index], (deep+1), index);
//         }
//         mes+=pre+'}';
//     } else {
//         mes+=pre+index+variable;
//     }
//     if (deep) {return mes;} else {alert(mes);}
//
// }

function lg(arr, level = null) {
    var result = dump(arr, level);
    result = "<pre>" + result + "</pre>"
    var openWin = window.open("about:blank", result, "width=400,height=400");
    openWin.document.write(result);
    // return alert(result);
}

function dump(arr, level) {

    var dumped_text = "";
    if(!level) level = 0;

    var level_padding = "";
    for(var j=0; j < level+1; j++) level_padding += "  ";

    if(typeof(arr) == 'object') {
        for(var item in arr) {
            var value = arr[item];
            if(typeof(value) == 'object') {
                dumped_text += level_padding + "'" + item + "' ...\n";
                dumped_text += dump(value,level+1);
            } else {
                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
            }
        }
    } else {
        dumped_text = "===>" +arr+ "<===("+ typeof(arr) + ")";
    }

    return dumped_text;
}
