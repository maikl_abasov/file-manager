function lg(arr) {

    var result = ' ---> Нет результатов <---';
    if (arr) result = _printf(arr); // --- формируем строку из массива
    _openWindow(result);            // --- показываем результат в новом окне

    // --- формируем строку из массива
    function _printf(arr) {

        if (typeof(arr) !== 'object')
            return '<ul><li>' + arr + '</li></ul>';

        var strResult = '';
        var deLimiter = ' => ';

        for (var i in arr) {
            var values = arr[i];
            var subValues = '';
            var li = deLimiter;
            if (typeof(values) == 'object') {
                subValues = _printf(values);
            } else {
                li += values;
            }
            strResult += '<li>[' + i + ']' + li + '</li>' + subValues;
        }

        return '<ul>' + strResult + '</ul>';
    }

    // --- показываем результат в новом окне
    function _openWindow(result, href) {
        var modal = window.open('', '', 'scrollbars=1, width=700,height=500');
        var style = `button { padding:10px; margin:10px; border:0px grey solid; width:30%; cursor:ponter  }
                     .lg-view-result { border:2px red solid; }`;

        var html = `<!DOCTYPE html><head><style>${style}</style><head>
                    <p><button onclick="window.close();" >Close</button></p><hr>
                    <p class="lg-view-result" ><pre>${result}</pre></p>`;
        modal.document.body.innerHTML = html;
    }
}



// function lg(arr, level = null) {
//     var result = var_dump(arr, level);
//     result = "<pre>" + result + "</pre>"
//     var openWin = window.open("about:blank", result, "width=700,height=500");
//     openWin.document.write(result);
// }
//
// function var_dump(arr, level) {
//     var dumped_text = "";
//     if(!level) level = 0;
//     var level_padding = "";
//     for(var j=0; j < level+1; j++) level_padding += "  ";
//     if(typeof(arr) == 'object') {
//         for(var item in arr) {
//             var value = arr[item];
//             if(typeof(value) == 'object') {
//                 dumped_text += level_padding + "'" + item + "' ...\n";
//                 dumped_text += var_dump(value,level+1);
//             } else {
//                 dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
//             }
//         }
//     } else {
//         dumped_text = "===>" +arr+ "<===("+ typeof(arr) + ")";
//     }
//     return dumped_text;
// }
//
// function print_r(obj, t){
//     var isArray = typeof(obj)==='array';
//     var tab = t || '';
//     var dump = isArray ? 'Array\n'+tab+'[\n' : 'Object\n'+tab+'{\n';
//     for(var i in obj){
//         if (obj.hasOwnProperty(i)) {
//             var param = obj[i];
//             var val = '';
//             var paramType = typeof(param);
//             switch(paramType){
//                 case 'array':
//                 case 'object':
//                     val = print_r(param,tab+'\t');
//                     break;
//                 case 'boolean':
//                     val = param ? 'true' : 'false';
//                     break;
//                 case 'string':
//                     val = '\''+param+'\'';
//                     break;
//                 default:
//                     val = param;
//             }
//             dump += tab+'\t'+i+' => '+val+',\n';
//         }
//     }
//     dump = dump.substring(0, dump.length-2)+'\n'+tab;
//     return isArray ?  dump+']' : dump+'}';
// };
