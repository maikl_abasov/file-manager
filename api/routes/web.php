<?php

use App\Http\Controllers\FtpController;
use Illuminate\Container\Container;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\FileManagerController;

$router = app('router'); //-- Illuminate\Routing\Router $router

$router->get('/', function () {
    $data = ['message' => 'Hello home'];
    $app = Container::getInstance();
    $factory = $app->make('view');
    return $factory->make('home')->with('data', $data);
});

$router->group(['prefix' => BASE_URI], function () use($router) {

    $router->get('/get-root-paths', [FileManagerController::class, 'getRootPaths']);
    $router->get('/scandir', [FileManagerController::class, 'scandir']);
    $router->get('/create-resource', [FileManagerController::class, 'createResource']);
    $router->post('/upload-file', [FileManagerController::class, 'uploadFile']);
    $router->get('/delete-resource', [FileManagerController::class, 'deleteResource']);
    $router->get('/get-file-content', [FileManagerController::class, 'getFileContent']);
    $router->post('/save-file-content', [FileManagerController::class, 'saveFileContent']);
    $router->get('/rename-file', [FileManagerController::class, 'renameFile']);
    $router->get('/download-file', [FileManagerController::class, 'downloadFile']);
    $router->get('/copy-file/{action}', [FileManagerController::class, 'copyFile']);

    $router->get('/ftp/scandir', [FtpController::class, 'scandir']);
    $router->get('/ftp/{action}', [FtpController::class, 'ftpAction']);

    $router->get('/get-dir-size', function(Request $request)  {

         $path = $request->query->get('path');
         $path = $path ?? '/';

         $bytes = disk_free_space($path);
         $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
         $base = 1024;
         $class = min((int)log($bytes , $base) , count($si_prefix) - 1);

         return (new \App\Http\Controllers\Controller())->json([
             'bytes' => $bytes,
             'size'  => sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class]
         ]);
    });

    $router->get('/test', function(Request $request) {

        // $url = 'http://test.ru/FILE-MANAGER/api-v2/ftp/copy?ftp_path=/maikl.21090.aqq.ru/tmp&local_path=C:/OSPanel/domains/test.ru/websocket-client';
        $data = $error = []; $code = 200;
        $path = 'C:/OSPanel/domains/my-web-service/totum-mit-master/totum/models';
        $path = 'C:/OSPanel/domains/my-web-service/useJquery.js';

        //$filesystem = new \Illuminate\Filesystem\Filesystem();
        // $data = $filesystem->allFiles($path);

        $status = false;
        // $ftpPath = '/maikl.21090.aqq.ru/tmp'

        $ftp = new \App\Services\Ftp\FtpClient([
            'host' => 'localhost',
            'user' => 'ftp',
            'password' => 'ftp',
            'port' => 21,
            'timeout' => 90,
        ]);

        $FTP_PATH = '/public_html/test-directory';
        $LOCAL_PATH = 'C:/OSPanel/domains/test.ru/websocket-client';

        // $status = $ftp->download($LOCAL_PATH, $FTP_PATH); //--- копирование папки с ftp сервера
        // $status = $ftp->download($LOCAL_PATH, $FTP_PATH . '/main.js'); //--- копирование файла с ftp сервера

        // $status = $ftp->upload($LOCAL_PATH . '/ApiControllerTest.php', $FTP_PATH); //--- загрузка файла с локального на ftp сервера
        // $status = $ftp->upload($LOCAL_PATH, $FTP_PATH); //--- загрузка папки  на ftp сервера

        // $status = $ftp->remove($FTP_PATH . '/main.js'); //--- удаление файла на ftp сервере
        // $status = $ftp->remove($FTP_PATH . '/websocket-client'); //--- удаление папки на ftp сервере

        lg([scandir('/')]);
    });
});



