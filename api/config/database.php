<?php

return [
    'driver'    => getenv('DB_CONNECTION', 'mysql'),
    'host'      => getenv('DB_HOST', '127.0.0.1'),
    'database'  => getenv('DB_DATABASE'),
    'username'  => getenv('DB_USERNAME', 'root'),
    'password'  => getenv('DB_PASSWORD', ''),
    'prefix'    => getenv('DB_PREFIX', ''),
    'charset'   => "utf8",
    'collation' => 'utf8_general_ci',
];
