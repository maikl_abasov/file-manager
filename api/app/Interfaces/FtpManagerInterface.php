<?php

namespace App\Interfaces;

interface FtpManagerInterface
{
    public function getList(string $path = '/', bool $recursive = false, string $filter = 'sort') : array;
}
