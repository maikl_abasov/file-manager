<?php

namespace App\Interfaces;

interface FileManagerInterface
{
    public function getList(string $path = '', int $dept = 0) : array;
}
