<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Container\Container;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function welcome(Request $request)
    {
        $data = [];
        $app = Container::getInstance();
        $factory = $app->make('view');
        return $factory->make('welcome')->with('data', $data);
    }

    public function user(int $id, Request $request, User $user)
    {
        $data = ['id' => $id];
        return $this->view('user', $data);
    }
}
