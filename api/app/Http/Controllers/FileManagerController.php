<?php

namespace App\Http\Controllers;

use App\Interfaces\FileManagerInterface;
use App\Services\FileService\FileManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FileManagerController extends Controller
{

    protected FileManagerInterface $fileManager;

    public function __construct(FileManagerInterface $fileManager) {
        parent::__construct();
        $this->fileManager = $fileManager;
    }

    public function scandir(Request $request) : JsonResponse
    {
        $path = $request->get('path');
        $serverRoot = $request->server->get('DOCUMENT_ROOT');
        $path = $path ? $path : $serverRoot;
        $files = $this->fileManager->getList($path);
        return $this->json($files);
    }

    //--- получить корневые директории (server root, system root)
    public function getRootPaths(Request $request) : JsonResponse {
        $serverRoot = $request->server->get('DOCUMENT_ROOT');
        $paths = explode('/', trim($serverRoot, '/'));
        $systemRoot = $paths[0];
        return $this->json([
            'server' => $serverRoot,
            'system' => $systemRoot,
        ]);
    }

    public function createResource(Request $request) : JsonResponse {

        $path = $request->query->get('path');
        $name = $request->query->get('name');
        $type = $request->query->get('type');

        $status = null;
        switch ($type) {
            case 'create-folder': $status = $this->fileManager->createDir($path, $name); break;
            case 'create-file'  : $status = $this->fileManager->createFile($path, $name); break;
        }
        return $this->json(['status' => $status]);
    }

    public function uploadFile(Request $request) : JsonResponse {
        $file = $request->file('file');
        $path = $request->query->get('path');
        $status  = $this->fileManager->uploadFile($path, $file);
        $message = $status ? 'Файл спешно загружен' : 'Не удалось загрузить';
        return $this->json(['status' => $status, 'message' => $message]);
    }

    public function deleteResource(Request $request) : JsonResponse {
        $path = $request->query->get('path');
        $type = $request->query->get('type');

        $status = null;
        switch ($type) {
            case 'dir'   : $status = $this->fileManager->removeDir($path); break;
            case 'file'  : $status = $this->fileManager->removeFile($path); break;
        }
        return $this->json(['status' => $status]);
    }

    public function getFileContent(Request $request) : JsonResponse {
        $path = $request->query->get('path');
        $data = $this->fileManager->getFileContent($path);
        return $this->json($data);
    }

    public function saveFileContent(Request $request) : JsonResponse {
        $path = $request->query->get('path');
        $content = $request->request->get('content');
        $status = $this->fileManager->getFilesystem()->put($path, $content);
        return $this->json($status);
    }

    public function renameFile(Request $request) : JsonResponse {
        $path = $request->query->get('path');
        $oldName = $request->query->get('old_name');
        $newName = $request->query->get('new_name');
        $oldPath = $path . '/' . $oldName;
        $newPath = $path . '/' . $newName;
        $data = $this->fileManager->renameFile($oldPath, $newPath);
        return $this->json($data);
    }

    public function downloadFile(Request $request) : JsonResponse {
        $path = $request->query->get('path');
        $type = $request->request->get('type');
        $data = $this->fileManager->downloadFile($path);
        return $this->json($data);
    }

    public function copyFile(string $action, Request $request) : JsonResponse {
        $source = $request->query->get('source');
        $dest = $request->query->get('dest');
        $status = $this->fileManager->copyFile($source, $dest, $action);
        return $this->json($status);
    }

}
