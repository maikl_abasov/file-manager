<?php

namespace App\Http\Controllers;

use App\Interfaces\FtpManagerInterface;
use App\Services\Ftp\FtpClient;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FtpController extends Controller
{
    protected FtpManagerInterface $ftpClient;

    public function __construct(FtpManagerInterface $ftpClient) {

        parent::__construct();
        $this->ftpClient = $ftpClient;
    }

    public function scandir(Request $request) : JsonResponse {
        $path = $request->query->get('path');
        $path = (!$path) ? '/' : $path;
        $status = $this->ftpClient->getList($path);
        return $this->json($status);
    }

    public function ftpAction(string $action, Request $request) : JsonResponse {

        $status = false;
        $ftpPath = $request->query->get('ftp_path');

        switch ($action) {
            case 'copy' :
                $localPath = $request->query->get('local_path');
                $status = $this->ftpClient->upload($localPath, $ftpPath);
                break;

            case 'download' :
                $localPath = $request->query->get('local_path');
                $status = $this->ftpClient->download($localPath, $ftpPath);
                break;

            case 'delete' :
                $status = $this->ftpClient->remove($ftpPath);
                break;
        }

        return $this->json($status);
    }
}
