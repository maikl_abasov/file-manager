<?php

namespace App\Http\Controllers;

use Illuminate\Container\Container;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class Controller
{

    protected Container $app;

    public function __construct() {
        $this->app = Container::getInstance();
    }

    public function view(string $page, mixed $data = null) : View
    {
        $factory = $this->app->make('view');
        return $factory->make($page)->with('data', $data);
    }

    public function json(mixed $data, mixed $error = null, int $code = 200,) : JsonResponse
    {
        return new JsonResponse(data: [
            'result' => $data, 'error' => $error
        ], status: $code);
    }

}
