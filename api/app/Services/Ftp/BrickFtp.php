<?php


namespace App\Services\Ftp;

use Exception;
use Brick\Ftp\FtpClient;
use Brick\Ftp\FtpException;

class BrickFtp extends FtpClient
{

    public function listDirectory(string $path) : array
    {
        $this->throwIfNotConnected();

        $records = false;

        try {
            $records = $this->call(true, 'ftp_mlsd', $this->conn, $path);
        } catch (FtpException $e) {
            throw new Exception($e->getMessage());
        }

        if ($records === false) {
            return $this->basicListDirectory($path);
        }

        $result = [];


        foreach ($records as $facts) {
            $fileInfo = $this->mlsdFactsToFileInfo($facts);

            if ($fileInfo !== null) {
                $result[] = $fileInfo;
            }
        }

        return $result;
    }

}
