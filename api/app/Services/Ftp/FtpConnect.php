<?php

namespace App\Services\Ftp;

class FtpConnect
{

    protected $connect;

    public function __construct(string $host, $port = 21, $timeout = 90){
        $this->connect = ftp_connect($host, $port, $timeout);
    }

    public function __call($func, $args){

        if(strstr($func,'ftp_') !== false && function_exists($func)){
            array_unshift($args, $this->connect);
            return call_user_func_array($func, $args);
        } else {
            die("$func is not a valid FTP function");
        }
    }
}
