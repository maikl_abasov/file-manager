<?php

namespace App\Services\Ftp;

use App\Interfaces\FtpManagerInterface;
use Exception;

class FtpClient implements FtpManagerInterface
{
    protected mixed $ftp;
    protected mixed $auth;
    public array $config;

    public function __construct(array $config) {
        $this->config = $config;
        $this->connect();
    }

    public function setConfig(array $config) : void {
        $this->config = $config;
    }

    public function connect() : void {
        $port = !empty($this->config['port']) ? $this->config['port'] : 21;
        $timeout = !empty($this->config['timeout']) ? $this->config['timeout'] : 90;
        $this->ftp = new FtpConnect($this->config['host'], $port, $timeout);
        $this->auth = $this->ftp->ftp_login($this->config['user'], $this->config['password']);
        $this->ftp->ftp_pasv(true);
    }

    public function remove(string $path) : mixed {
        $status = false;
        if($this->isDir($path)) {
            $status =  $this->removeDir($path);
        } elseif($this->isFile($path)) {
            $status = $this->removeFile($path);
        }
        return $status;
    }

    //--- выгрузка с ftp сервер
    public function download(string $localPath, string $remotePath) : mixed {

        $status = false;

        if($this->isDir($remotePath)) {
            $list = explode('/', $remotePath);
            $fileName = end($list);
            $localPath = $localPath . '/' . $fileName;
            if(!is_dir($localPath)) mkdir($localPath, 0775);
            $status =  $this->downloadDir($localPath, $remotePath);
        } elseif($this->isFile($remotePath)) {
            $list = explode('/', $remotePath);
            $fileName = end($list);
            $status = $this->downloadFile($localPath . '/' . $fileName, $remotePath);
        }

        return $status;
    }

    //--- загрузка на ftp сервер
    public function upload(string $localPath, string $remotePath) : mixed {

        if(!file_exists($localPath)) return false;

        if(is_file($localPath)) {
            $fileName = pathinfo($localPath, PATHINFO_BASENAME);
            $status = $this->uploadFile($localPath, $remotePath . '/' . $fileName);
        } else {
            $list = explode('/', $localPath);
            $fileName = end($list);
            // dd([$localPath, $remotePath . '/' . $fileName]);
            $status = $this->uploadDir($localPath, $remotePath . '/' . $fileName);
        }

        return $status;
    }

    //--- копирование папки с ftp сервера
    public function downloadDir(string $localPath, string $remotePath) : mixed {

        $status = false;

        $files = $this->ftp->ftp_nlist($remotePath);

        foreach ($files as $remoteFile) {
            $list = explode('/', $remoteFile);
            $fileName = end($list);
            if($fileName == '.' || $fileName == '..') continue;
            $localFile = $localPath . '/' . $fileName;

            if ($this->isFile($remoteFile)) {
                $status = $this->downloadFile($localFile, $remoteFile);
            } else {
                if(!is_dir($localFile)) mkdir($localFile, 0775);
                $this->downloadDir($localFile, $remoteFile);
            }
        }

        return $status;
    }

    //--- копирование файла с ftp сервера
    public function downloadFile(string $localFile, string $remoteFile) : bool {
        $status = $this->ftp->ftp_get($localFile, $remoteFile, FTP_BINARY);
        return $status;
    }

    // Удаление директорию со всем содержимым
    public function removeDir(string $path) : array
    {
        if(!$this->isDir($path)) {
            return [
                'status' => false,
                'message' => 'Директория: ' . $path. ' не существует'
            ];
        }

        $fileList = $this->ftp->ftp_nlist($path);

        foreach ($fileList as $file) {
            $list = explode('/', $file);
            $fileName = end($list);
            if($fileName == '.' || $fileName == '..') continue;

            if ($this->isFile($file)) $this->ftp->ftp_delete($file);
            else $this->removeDir($file);
        }

        $status = $this->ftp->ftp_rmdir($path);
        $message = ($status) ? $message = 'Директория успешно удалена' :
                             'Директорию: ' . $path . ' не удалось удалить';
        return ['status' => $status, 'message' => $message,];
    }

    public function removeFile(string $path) : bool {
        return $this->ftp->ftp_delete($path);
    }

    // Проверка наличия директории
    public function isDir(string $directory) : bool
    {
        $status  = false;
        $origin = $this->ftp->ftp_pwd();
        if (@$this->ftp->ftp_chdir($directory)) {
            $this->ftp->ftp_chdir( $origin);
            $status = true;
        }
        return $status;
    }

    // Проверка наличия файла
    public function isFile(string $path) : bool
    {
        return  ($this->ftp->ftp_size($path) != -1) ? true : false;
    }

    // --- Получить файлы
    public function getList(string $path = '/', bool $recursive = false, string $filter = 'sort') : array
    {
        $list = $this->ftp->ftp_mlsd($path);
        $files = $directories = [];
        $path = ($path == '/') ? '' : $path;

        foreach ($list as $file) {
            if($file['name'] == '.' || $file['name'] == '..') continue;

            $realPath = $path . '/' . $file['name'];
            $file['real_path'] = $realPath;
            if($file['type'] == 'dir') $directories[] = $file;
            else $files[] = $file;
        }

        $result = array_merge($directories, $files);

        return $result;
    }

    public function create(string $path, $type = 'dir', mixed $content = '') : array {
        switch ($type) {
            case 'file' : $result = $this->createFile($path, $content);
            default     : $result =  $this->createDir($path);
        }
        return $result;
    }

    //--- Cоздания директории
    public function createDir(string $path) : array {

        $status  = false;
        $message = 'Не удалось создать директорию: ' .$path;
        if ($this->ftp->ftp_mkdir($path)) {
            $status  = true;
            $message = 'Директория: "'.$path.'" создана';
        }
        return ['status' => $status, 'message' => $message,];
    }

    //--- Cоздание файла
    public function createFile(string $path, mixed $content = '') : array {

        $status  = false;
        $message = 'Не удалось создать файл: ' . $path;
        $file = fopen('php://temp', 'r+');
        fwrite($file, $content);
        rewind($file);
        if ($this->ftp->ftp_fput($path, $file, FTP_ASCII)) { // Создание файла на сервере
            $status  = true;
            $message = 'Файл : "'.$path.'" создана';
        }
        return ['status' => $status, 'message' => $message,];
    }

    // Закачать папку с файлами на сервер
    public function uploadDir(string $localPath, string $remotePath) : bool {

        if(!$this->isDir($remotePath)) $this->create($remotePath, 'dir');

        $files  = scandir($localPath);
        $status = false;

        foreach($files as $file) {
            if ($file == '.' || $file == '..') continue;
            $source = $localPath . '/' . $file;
            $remote = $remotePath . '/' . $file;
            if (is_file($source)) {
                $this->uploadFile($source, $remote);
            } else {
                $this->uploadDir($source, $remote);
            }
            $status = true;
        }

        return $status;
    }

    public function uploadFile(string $localPath, string $remotePath) : bool {
        $status = false;
        if(file_exists($localPath)) {
            $localFp = fopen($localPath, 'r');
            $status = $this->ftp->ftp_fput($remotePath, $localFp);
        }
        return $status;
    }

    // Переименовать файл или папку
    public function rename(string $oldName, string $newName): bool {
        return ($this->ftp->ftp_rename ($oldName, $newName)) ? true : false;
    }
}
