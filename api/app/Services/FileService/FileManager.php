<?php

namespace App\Services\FileService;

use App\Interfaces\FileManagerInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use ZipArchive;

class FileManager implements FileManagerInterface
{
    protected string $path;
    public Finder $finder;
    public Filesystem $filesystem;

    public function __construct(string $path = '') {
        $this->setPath($path);
        $this->finder = new Finder();
        $this->filesystem = new Filesystem();
    }

    public function getFinder() : mixed {
        return $this->finder;
    }

    public function getFilesystem() : mixed {
        return $this->filesystem;
    }

    public function setPath(string $path) : self {
        $this->path = $path;
        return $this;
    }

    public function createDir(string $path, string $name): bool {
        $filePath = $path .'/'. $name;
        return $this->filesystem->makeDirectory($filePath);
    }

    public function createFile(string $path, string $name, mixed $content = '') : bool {
        $filePath = $path .'/'. $name;
        return $this->filesystem->append($filePath, $content);
    }

    public function removeDir(string $path): bool {
        return $this->filesystem->deleteDirectory($path);
    }

    public function removeFile(string $path) : bool {
        return $this->filesystem->delete($path);
    }

    public function uploadFile(string $path, UploadedFile $file): bool {
        $originalName = $file->getClientOriginalName();
        $filePath = $path .'/'. $originalName;
        $tmpPath  = $file->getRealPath();
        return move_uploaded_file($tmpPath, $filePath) ? true : false;
    }

    //--- получить файлы и папки из директории  | $dept - уровень рекурсии
    public function getList(string $path = '', int $dept = 0) : array {
        /** @var Finder $finder */
        $finder = $this->getFinder();
        $iterator = $finder->depth($dept)->in($path);
        $files = [];
        foreach ($iterator as $file) {
            $files[] = $this->setFile($file);
        }
        return $files;
    }

    //--- получить только файлы из директории рекурсивно
    public function getFiles(string $path = '', int $dept = 0) : array {
        /** @var Finder $finder */
        $finder = $this->getFinder();
        if($dept)  $iterator = $finder->depth($dept)->in($path);
        else $iterator = $finder->files()->in($path);

        $files = [];
        foreach ($iterator as $file) {
            $files[] = $this->setFile($file);
        }
        return $files;
    }

    protected function setFile(SplFileInfo $file) : array {
        /** @var SplFileInfo $file */
        $type = $file->isDir() ? 'dir' : 'file';
        $realPath = str_replace('\\', '/', $file->getRealPath());
        $fileName = $file->getFilename();
        $ext = $file->getExtension();
        $path = $file->getPath();

        $fileInfo = [
            'type'      => $type,
            'real_path' => $realPath,
            'filename'  => $fileName,
            'name'      => $fileName,
            'path'      => $path,
            'ext'       => $ext,
        ];

        try {

            $fileInfo['group'] = $file->getGroup();
            $fileInfo['owner'] = $file->getOwner();
            $fileInfo['size'] = $file->getSize();
            $fileInfo['perms'] = $file->getPerms();
            $fileInfo['aTime'] = $file->getATime();

        }catch (\Exception $e) {
            $message = $e->getMessage();
            $fileInfo['group'] = $message;
            $fileInfo['owner'] = $message;
            $fileInfo['size']  = $message;
            $fileInfo['perms'] = $message;
            $fileInfo['aTime'] = $message;
        }

        return $fileInfo;
    }

    public function removeDirectoryRecursive(string $path) : mixed
    {
        if(!is_dir($path)) return unlink($path);
        $files = new \FilesystemIterator($path);
        foreach ($files as $file) {
            if (is_dir($file) && !is_link($file)) $this->removeDirectoryRecursive($file);
            else unlink($file);
        }
        return rmdir($path);
    }

    public function getFileContent(string $path) : mixed {
        $types = ['jpg', 'JPG', 'png', 'PNG', 'JPEG', 'jpeg'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = $this->filesystem->get($path);
        if(in_array($type, $types)) $data = 'data:' . $type . ';base64,' . base64_encode($data);
        return $data;
    }

    public function renameFile(string $oldName, string $newName) : bool {
        return ($this->filesystem->isDirectory($oldName)) ?
                    $this->filesystem->moveDirectory($oldName, $newName) :
                    $this->filesystem->move($oldName, $newName);
    }

    public function downloadFile(string $path) : mixed {

        $name = $this->filesystem->name($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);

        if($this->filesystem->isFile($path)) {
            $data = file_get_contents($path);
        } else {
            $zipFile = $this->createArchiveDirRecursive($path);
            $data = file_get_contents($zipFile);
            $path = $zipFile;
        }

        $data = 'data:' . mime_content_type($path) . ';base64,' . base64_encode($data);

        return $data;
    }

    protected function createArchiveDirRecursive(string $path) : string {

        $zipFile = 'download.zip';
        $zip = new ZipArchive();

        $zip->open($zipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $file) {
            if ($file->isDir()) continue;
            $realPath = $file->getRealPath();
            $relativePath = substr($realPath, strlen($path) + 1);
            $zip->addFile($realPath, $relativePath);
        }

        $zip->close();

        return $zipFile;
    }

    public function copyFile(string $source, string $dest, string $action) : mixed {
        if($this->filesystem->isFile($source)) {
           $name = $this->filesystem->name($source);
           $ext  = $this->filesystem->extension($source);
           $dest = $dest . '/' . $name . '.' . $ext;
           $status = ($action == 'move') ?
                        $this->filesystem->move($source, $dest) :
                        $this->filesystem->copy($source, $dest);
        } else {
            $status = ($action == 'move') ?
                   $this->filesystem->moveDirectory($source, $dest):
                   $this->filesystem->copyDirectory($source, $dest);
        }
        return $status;
    }

}
