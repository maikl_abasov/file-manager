<?php

namespace Core;

use Illuminate\Container\Container;
use Illuminate\Http\Request;

use Core\Providers\RouterServiceProvider;
use Core\Providers\ViewServiceProvider;
use Core\Providers\DatabaseServiceProvider;
use Core\Providers\SystemServiceProvider;

class AppKernel
{

    protected Container $app;
    protected Request $request;

    protected $providers = [
        SystemServiceProvider::class,
        RouterServiceProvider::class,
        DatabaseServiceProvider::class,
        ViewServiceProvider::class,
    ];

    protected $middlewares = [];

    public function __construct(Container $app, string $rootPath) {
        $this->app = $app;
        $this->app['ROOT_PATH']   = $rootPath;
        $this->app['CONFIG_PATH'] = $rootPath . '/config';
        Container::setInstance($this->app);
        $this->init();
    }

    public function init() : void {
        $this->initProviders();
        $this->request = $this->app->get(Request::class);
        $this->initMiddlewares();
    }

    public function initProviders() : void {
        foreach ($this->providers as $provider) {
            $object = new $provider($this->app);
            $object->boot();
            $object->register();
        }
    }

    public function initMiddlewares() : void {
        foreach ($this->middlewares as $middleware) {
            $object = new $middleware($this->request);
            $object->handle();
        }
    }

    public function handle() {
        $response = $this->app['router']->dispatch($this->request);
        $response->send();
    }

}
