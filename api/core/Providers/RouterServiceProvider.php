<?php


namespace Core\Providers;

use Illuminate\Events\EventServiceProvider;
use Illuminate\Routing\RoutingServiceProvider;
use Illuminate\Support\ServiceProvider;

class RouterServiceProvider extends \Core\Providers\ServiceProvider implements ServiceProviderInterface
{
    public function boot() : void
    {
    }

    public function register() : void
    {
        (new EventServiceProvider($this->app))->register();
        (new RoutingServiceProvider($this->app))->register();
        require ROOT_PATH . '/routes/web.php';
    }
}
