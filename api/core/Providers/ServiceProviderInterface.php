<?php

namespace Core\Providers;

interface ServiceProviderInterface
{
    /**
     * Регистрация любых служб приложения.
     *
     * @return void
     */
    public function register() : void;

    /**
     * Загрузка любых служб приложения.
     * @return void
     */
    public function boot() : void;

}
