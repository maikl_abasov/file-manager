<?php

namespace Core\Providers;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider implements ServiceProviderInterface
{
    public function boot() : void
    {
    }

    public function register() : void
    {
        $config = config('database');
        $manager = new Manager();
        $manager->addConnection($config);
        $manager->bootEloquent();

    }
}
