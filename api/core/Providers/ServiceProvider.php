<?php

namespace Core\Providers;

use Illuminate\Container\Container;

class ServiceProvider
{

    protected Container $app;

    public function __construct(Container $app) {
        $this->app = $app;
    }

}
