<?php

namespace Core\Providers;

use Illuminate\Support\Fluent;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends \Core\Providers\ServiceProvider implements ServiceProviderInterface
{

    public function boot() : void
    {
    }

    public function register() : void
    {

        $this->app->instance('config', new Fluent);
        $this->app['config']['view.compiled'] = ROOT_PATH . '/storage/framework/views';
        $this->app['config']['view.paths'] = [ROOT_PATH . '/resources/views'];

        (new \Illuminate\View\ViewServiceProvider($this->app))->register();
        (new \Illuminate\Filesystem\FilesystemServiceProvider($this->app))->register();

    }
}
