<?php

namespace Core\Providers;

use App\Interfaces\FileManagerInterface;
use App\Interfaces\FtpManagerInterface;
use App\Services\FileService\FileManager;
use App\Services\Ftp\FtpClient;
use Illuminate\Http\Request;

class SystemServiceProvider extends ServiceProvider implements ServiceProviderInterface
{

    public function boot() : void
    {
    }

    public function register() : void
    {
        $request = Request::createFromGlobals();
        $this->app->instance(Request::class, $request);

        $rootPath = $this->app->get('ROOT_PATH');
        $configPath = $this->app->get('CONFIG_PATH');

        $this->loadEnv($rootPath);
        $this->setConfig($configPath);

        $this->app->bind(FtpManagerInterface::class, function() {
            $ftpConfig = config('ftp');
            return new FtpClient($ftpConfig);
        });

        $this->app->bind(FileManagerInterface::class, FileManager::class);
    }

    public function loadEnv(string $path) : void
    {
        $evnData = file($path . '/.env');
        foreach ($evnData as $line) {
            if(!$line) continue;
            $item = explode('=', $line);
            if(!empty($item[0]) && isset($item[1])) {
                $key = trim($item[0]);
                $value = trim($item[1]);
                putenv(sprintf('%s=%s', $key, $value));
                $_ENV[$key] = $value;
                $_SERVER[$key] = $value;
            }
        }
    }

    public function setConfig(string $path) : void
    {
        $configFiles = glob($path . "/*.php");
        $configs = [];
        foreach ($configFiles as $file) {
           $config = require $file;
           $name = pathinfo($file, PATHINFO_FILENAME);
           $configs[$name] = $config;
        }

        $this->app['settings'] = $configs;
    }

}
