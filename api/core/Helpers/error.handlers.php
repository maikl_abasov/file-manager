<?php

use Core\Logger\Logger;

$errorHandleStatus = false;

function printError(array $data, string $errorMessage = '') {
    $data['full_message'] = $errorMessage;
    $data = json_encode(['error' => $data, 'result' => null], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    die($data);
}

function customErrorHandler($errno, $errstr, $errfile, $errline)
{
    global $errorHandleStatus;

    if (!(error_reporting() & $errno)) {
        // Этот код ошибки не включён в error_reporting,
        // так что пусть обрабатываются стандартным обработчиком ошибок PHP
        return false;
    }

    // может потребоваться экранирование $errstr:
    $errstr = htmlspecialchars($errstr);

    $errorMessage = '';

    switch ($errno) {
        case E_USER_ERROR:
            $errorMessage .= "<b>Пользовательская ОШИБКА</b> [$errno] $errstr<br />\n";
            $errorMessage .= "  Фатальная ошибка в строке $errline файла $errfile";
            $errorMessage .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            $errorMessage .= "Завершение работы...<br />\n";
            // exit(1);
            break;

        case E_USER_WARNING:
            $errorMessage .= "<b>Пользовательское ПРЕДУПРЕЖДЕНИЕ</b> [$errno] $errstr<br />\n";
            break;

        case E_USER_NOTICE:
            $errorMessage .= "<b>Пользовательское УВЕДОМЛЕНИЕ</b> [$errno] $errstr<br />\n";
            break;

        default:
            $errorMessage .= "Неизвестная ошибка: [$errno] $errstr<br />\n";
            break;
    }

    $errorHandleStatus = true;
    $data = ['type' => $errno, 'message' => $errstr, 'file' => $errfile, 'line' => $errline, 'error' => 'Error'];
    Logger::fatal($errorMessage, $data);

    printError($data);

    /* Не запускаем внутренний обработчик ошибок PHP */
    // return true;
}

function customExceptionHandler(\Throwable $err)  {

    global $errorHandleStatus;

    if(!$errorHandleStatus) {
        $errorMessage = "Неперехваченное исключение: " . $err->getMessage();
        $data = ['type' => $err->getCode(), 'message' => $err->getMessage(), 'file' => $err->getFile(), 'line' => $err->getLine(), 'error' => 'Exception'];
        Logger::fatal($errorMessage, $data);
    } else {
        $errorHandleStatus = false;
    }

    printError($data, $errorMessage);
}
