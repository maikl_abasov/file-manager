<?php

use Illuminate\Container\Container;

if (! function_exists('app')) {

    function app(mixed $key, array $params = []) : mixed
    {
        $app = Container::getInstance();
        if(!$app->has($key))return null;
        return $app->get($key);
    }
}

if (! function_exists('lg')) {

    function lg() : mixed
    {
       $out = print_r(func_get_args(), true);
       echo "<pre>{$out}</pre>";
       die();
    }
}

if (! function_exists('config')) {

    function config(string $key) : mixed
    {
       $config =  app('settings');
       return isset($config[$key]) ? $config[$key] : null;
    }
}

if (! function_exists('ExceptionHandler')) {

    function ExceptionHandler(mixed $e) : mixed
    {

        $error = [
            'status' => true,
            'message' => $e->getMessage(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'code' => $e->getCode(),
        ];

        $response = json_encode([
            'error' => $error,
            'result' => null,
        ]);

        return $response;
    }
}
