<?php

namespace Core\Container;

use Illuminate\Container\Container;
use ReflectionClass;
use Exception;

class ServiceContainer extends Container
{
   public function make($abstract, array $parameters = []) {
       if(!empty($parameters)) $data = $parameters;
       else $data = $this->resolveArguments($abstract);
       return parent::make($abstract, $data);
   }

    public function resolveArguments($class)
    {
        $reflectedClass = new ReflectionClass($class);
        $reflectedConstructor = $reflectedClass->getConstructor();
        if (empty($reflectedConstructor)) return [];

        $parameters = $reflectedConstructor->getParameters();

        $arguments = [];

        foreach ($parameters as $key => $param) {

            $hasType = $param->hasType();
            $argName = $param->name;

            if($this->has($argName)) {
                $arguments[$argName] = $this->get($argName);
            }
        }

        return $arguments;
    }
}
