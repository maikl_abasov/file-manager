<?php

// session_start();

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

$uriList = explode('/public', trim($_SERVER['SCRIPT_NAME']));
$baseUrl = (!empty($uriList[0])) ? $uriList[0] : '';

define('ROOT_PATH', dirname(__DIR__));
define('STORAGE_PATH', ROOT_PATH . '/storage');
define('BASE_URI', $baseUrl);

require  ROOT_PATH . '/vendor/autoload.php';

// use Core\Container\ServiceContainer;
use Core\AppKernel;
use Core\Logger\Logger;
use Illuminate\Container\Container;

Logger::getInstance(STORAGE_PATH);

set_error_handler('customErrorHandler');
set_exception_handler('customExceptionHandler');

$kernel = new AppKernel(new Container, ROOT_PATH);

try {
    $kernel->handle();
} catch (Exception $e) {
    $response = ExceptionHandler($e);
    die($response);
}
