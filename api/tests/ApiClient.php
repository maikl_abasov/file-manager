<?php

namespace Tests;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\NativeResponse;
// use GuzzleHttp\Client;

class ApiClient
{

    public mixed $client;
    public string $apiUrl  = '';
    public string $ROOT_DIR;
    public mixed $content;
    public mixed $response;
    public mixed $jsonData;
    public mixed $data;
    public int   $statusCode = 200;

    public function __construct(string $apiUrl = '')
    {
        $this->apiUrl = $apiUrl;
        $this->client = HttpClient::create();
        $this->ROOT_DIR = dirname(__DIR__);
        $this->loadEnv();
        if(!$this->apiUrl) {
            $this->apiUrl = getenv('APP_URL');
        }
    }

    public function loadEnv(): void
    {
        $evnData = file($this->ROOT_DIR . '/.env');
        foreach ($evnData as $line) {
            if(!$line) continue;
            $item = explode('=', $line);
            if(!empty($item[0]) && isset($item[1])) {
                $key = trim($item[0]);
                $value = trim($item[1]);
                putenv(sprintf('%s=%s', $key, $value));
                $_ENV[$key] = $value;
                $_SERVER[$key] = $value;
            }
        }
    }

    public function send($url, $method = 'GET', mixed $data = [], array $headers = []): NativeResponse
    {
        $url = $this->apiUrl . $url;

        switch ($method) {
            case 'GET' :
                $response = $this->client->request('GET', $url);
                break;

            case 'POST' :
                $response = $this->client->request('POST', $url, $data);
                break;
        }

        $this->statusCode = $response->getStatusCode();
        $this->response   = $response;
        $this->content    = $response->getContent();
        $this->jsonData   = (array)json_decode($this->content);
        $this->data  = $response->toArray();

        return $this->response;
    }

    public function getClient() : mixed {
        return $this->client;
    }

    public function getJsonData() : mixed{
        return $this->jsonData;
    }

    public function printData() : mixed {
        echo print_r($this->response, true);
    }

}
