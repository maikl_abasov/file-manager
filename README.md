### ТЕСТОВОЕ ПРИЛОЖЕНИЕ FILE-MANAGER
### Управление файлами + ftp


### Приложение состоит из 2 частей
backend лежит в папке api, 

php с использованием пакетов Illuminate
```
cd api/composer install
api/env.txt переименовать в .env
```

### front написан на vuejs 3 composition api, папка front
связь между частями через api url в /front/src/api/http.js

const API_URL = 'http://test.ru/FILE-MANAGER/api';
```
cd front/npm install
npm run serve
```
